<?php 
session_start()
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="UIdeck">
    <title></title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/line-icons.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.css">
    <link rel="stylesheet" href="assets/css/slicknav.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/main.css">    
    <link rel="stylesheet" href="assets/css/responsive.css">

  </head>
  
  <body>
    <?php
    if(!($_SESSION["username"])){
    echo"<script>window.location.href='login.php ';</script>";
  }

include 'connexion.php'; 

 if (isset($_POST['rechercher'])){

 $infos= ($_POST["infos"]);
  $lieu= ($_POST["lieu"]);
  $categorie= ($_POST["categorie"]);

   $requete = mysqli_query($connect,"select * from fournisseur where nom  LIKE '%{$infos}%' and ville LIKE '%{$lieu}%' ");
    if( mysqli_num_rows($requete)>0){

        $_SESSION["infos"]=$infos;
       $_SESSION["lieu"]=$lieu;

       echo"<script>window.location.href='rechercheResultat.php';</script>";

    }
     else{
      echo"<script>window.location.href='noResult.php';</script>";

     }

    }
 


 


    ?>

    <!-- Header Section Start -->
    <header id="home" class="hero-area"> 
      <!-- Navbar Start -->
     <!-- Navbar Start -->
     <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
      <div class="container">
        <div class="theme-header clearfix">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="lni-menu"></span>
              <span class="lni-menu"></span>
              <span class="lni-menu"></span>
            </button>
            <!-- <a href="index.html" class="navbar-brand"><img src="assets/img/logo.png" alt=""></a>-->
            <a href="#"><h1 style="color: #fed136; font-family: Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';">Catalog</h1></a>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item">
                
              </li>
              <a class="nav-link" href="inscription.php">
                  inserer
                </a>

              <li class="nav-item">
                <a class="nav-link" href="tt.html">
                  modifier
                </a>
              </li>
        
            <li class="button-group">
              <a href="compteEmploye.php" class="button btn btn-common"><?php echo $_SESSION["username"]?></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="mobile-menu" data-logo="assets/img/logo-mobile.png"></div>
  </nav>
      <!-- Navbar End -->
    
      <div class="container">      
        <div class="row space-100 justify-content-center">
          <div class="col-lg-10 col-md-12 col-xs-12">
            <div class="contents">
              <h2 class="head-title">Trouvez votre fournisseur</h2>
              <p>Saisissez tous les critères de recherche pour des résultat plus précis </p>
              <div class="job-search-form">
                <form method="post">
                  <div class="row">
                    <div class="col-lg-5 col-md-6 col-xs-12">
                      <div class="form-group">
                        <input class="form-control" type="text" placeholder="nom du fournisseur, compagnie ou mot clés" name="infos">
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                      <div class="form-group">
                       <input class="form-control" type="text" placeholder="ville" name="lieu">
                        <i class="lni-map-marker"></i>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-xs-12">
                      <div class="form-group">
                       <input class="form-control" type="text" placeholder="catégorie" name="categorie">
                        <i class="lni-layers"></i>
                      </div>
                    </div>
                    <div class="col-lg-1 col-md-6 col-xs-12">
                      <button type="submit" class="button" name="rechercher" ><i class="lni-search"></i></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div> 
      </div>             
    </header>
    <!-- Header Section End --> 

    <!-- Category Section Start -->
    <section class="category section bg-gray">
      <div class="container">
        <div class="section-header">  
          <h2 class="section-title">Rechercher par Categories</h2>
          <p>Most popular categories of portal, sorted by popularity</p>
        </div>
        </div>
      <div class="row">

        <?php 
        if(!($_SESSION["username"])){
          echo"<script>window.location.href='login.php ';</script>";
        }
        include 'connexion.php'; 

        $requete = mysqli_query($connect,"select * from fournisseur order by nom");


        while ($rslt = mysqli_fetch_row($requete)){
          $id=$rslt[0];
          
          $cat=$rslt[7];
            $courriel=$rslt[5];
            $tel=$rslt[4];
         
           $nom=$rslt[1];
          $adresse = $rslt[2];
            $ville=$rslt[3];
            $logo=$rslt[6];
            $lien='./uploads/';
            $img=$lien.$logo;
          $_SESSION["id"]=$id;

        





            ?>
            <div class="col-lg-6 col-md-12 col-xs-12">
              <a class="job-listings-featured" href="fournisseur.php">
                <div class="row">
                  <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="job-company-logo">
                      <img src="<?php echo $img ?>" alt="" height="70px" width="70px">
                    </div>
                    <div class="job-details">
                      <h3><?php echo $nom ?></h3>
                      <span class="company-neme"><?php echo $cat ?></span>
                      <div class="tags">  
                        <span><i class="lni-map-marker"></i><?php echo $adresse ?></span>  
                        <span><i class="lni-user"></i><?php echo $tel ?></span>   
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-xs-12 text-right">
                    <div class="tag-type">
                      <sapn class="heart-icon">
                        <i class="lni-heart"></i>
                      </sapn>
                      <span class="full-time"><?php echo $courriel ?></span>
                    </div>
                  </div>
                </div>
              </a>

            </div>
            
            
            <?php
          
        };
        
        


        ?>
        
        
      </div>
      </div>
    </section>
    <!-- Category Section End -->  

  

   

   
   
    <!-- Footer Section Start -->
    <footer>
     <div id="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="site-info text-center">
              <p>Designed and Developed by <a href="https://uideck.com" rel="nofollow">Christian Junior Djomga</a></p>
            </div>     
          </div>
        </div>
      </div>
         </div>
      </footer>
    <!-- Footer Section End -->  

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="lni-arrow-up"></i>
    </a> 

    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="assets/js/jquery-min.js"></script>
    <script src="assets/js/popper.min.js"></script>

    <script src="assets/js/owl.carousel.min.js"></script>     
    <script src="assets/js/jquery.slicknav.js"></script>     
    <script src="assets/js/jquery.counterup.min.js"></script>      
    <script src="assets/js/waypoints.min.js"></script>     
    <script src="assets/js/form-validator.min.js"></script>
    <script src="assets/js/contact-form-script.js"></script>   
    <script src="assets/js/main.js"></script>
    
  </body>
</html>