  <?php 
  session_start()
  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="author" content="UIdeck">
    <title>JobX - Bootstrap HTML5 Job Portal Template</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/line-icons.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.css">
    <link rel="stylesheet" href="assets/css/slicknav.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/main.css">    
    <link rel="stylesheet" href="assets/css/responsive.css">

  </head>

  <body>

    <!-- Header Section Start -->
    <style>
      .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
      }

      /* Modal Content */
      .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 80%;
      }

      /* The Close Button */
      .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
      }

      .close:hover,
      .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
      }
    </style>

    <?php 
    if(!($_SESSION["username"])){
      echo"<script>window.location.href='login.php ';</script>";
    }
    include 'connexion.php'; 


    $requete = mysqli_query($connect,"select * from fournisseur where id_four='".$_SESSION["id"] ."'") or die ("erreur de requete sql");


    while ($rslt = mysqli_fetch_row($requete)){
      $id=$rslt[0];
          
          $cat=$rslt[7];
            $courriel=$rslt[5];
            $tel=$rslt[4];
         
           $nom=$rslt[1];
          $adresse = $rslt[2];
            $ville=$rslt[3];
            $logo=$rslt[6];
            $lien='./uploads/';
            $img=$lien.$logo;
    }

  





     ?>
     <header id="home" class="hero-area"> 
      <!-- Navbar Start -->
     <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
      <div class="container">
        <div class="theme-header clearfix">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="lni-menu"></span>
              <span class="lni-menu"></span>
              <span class="lni-menu"></span>
            </button>
            <!-- <a href="index.html" class="navbar-brand"><img src="assets/img/logo.png" alt=""></a>-->
            <a href="acceuil.php"><h1 style="color: #fed136; font-family: Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';">Catalog</h1></a>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item">
                
              </li>
              <a class="nav-link" href="Apropos.php">
                  A propos
                </a>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="listeEmplois.php" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Mes Emplois
                  </a>
                </li>
                
                
                <li class="nav-item">
                  <a class="nav-link" href="contact.php">
                    Contact
                  </a>
                </li>
                 <li class="nav-item active">
                <a class="nav-link" href="login.html">Ajouter un fournisseur</a>
              </li>


                <li class="button-group">
                  <a href="compteEmployeur.php" class="button btn btn-common"><?php echo $_SESSION["username"]?></a>
                </li>

          
              </ul>
            </div>
                 
          </div>
        </div>
        
      </nav>
      <!-- Navbar End -->            
    </header>
    <!-- Header Section End --> 

    <!-- Page Header Start -->
    <div class="page-header">
      <div class="container">
        <div class="row">         
          <div class="col-lg-8 col-md-6 col-xs-12">
            <div class="breadcrumb-wrapper">
              <div class="img-wrapper">
                <img src="<?php echo $img ?>" alt="" height="150" width="150">
              </div>
                 
               
             
            </div>
          </div>
         
        </div>
      </div>
    </div>
    <!-- Page Header End --> 

    <!-- Job Detail Section Start -->  

    <section class="job-detail section">
      <div class="container">
        <div class="row justify-content-between">
          <div class="col-lg-8 col-md-12 col-xs-12">
            <div class="content-area"> 
            
                <h4>Nom du fournisseur</h4>
                <p><?php echo $nom ?>.</p>
                <h5>adresse</h5>
                <p><?php echo $adresse ?></p>
                <h4>ville</h4>
                <p><?php echo $ville ?>.</p>
                <h4>telephone</h4>
                <p><?php echo $tel ?>.</p>
                <h4>courriel</h4>
                <p><?php echo $courriel ?>.</p>
                
               
            </div>
          </div>
          <div class="col-lg-4 col-md-12 col-xs-12">
            <div class="sideber">
              <div class="widghet">
                <h3>Ville</h3>
                <div class="maps">
                  <div id="map" class="map-full">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d405691.57240383344!2d-122.3212843181106!3d37.40247298383319!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fb68ad0cfc739%3A0x7eb356b66bd4b50e!2sSilicon+Valley%2C+CA%2C+USA!5e0!3m2!1sen!2sbd!4v1538319316724" allowfullscreen=""></iframe>
                  </div>
                </div>
              </div>
             
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Job Detail Section End --> 


    <!-- Footer Section Start -->
      <footer>
     <div id="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="site-info text-center">
              <p>Designed and Developed by <a href="https://uideck.com" rel="nofollow">Christian Junior Djomga</a></p>
            </div>     
          </div>
        </div>
      </div>
         </div>
      </footer>
    <!-- Footer Section End -->  

    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="lni-arrow-up"></i>
    </a> 

    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="assets/js/jquery-min.js"></script>
    <script src="assets/js/popper.min.js"></script>

    <script src="assets/js/owl.carousel.min.js"></script>     
    <script src="assets/js/jquery.slicknav.js"></script>     
    <script src="assets/js/jquery.counterup.min.js"></script>      
    <script src="assets/js/waypoints.min.js"></script>     
    <script src="assets/js/form-validator.min.js"></script>
    <script src="assets/js/contact-form-script.js"></script>   
    <script src="assets/js/main.js"></script>

  </body>
  </html>