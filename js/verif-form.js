$(document).ready(function(){
    $("#btnSub").click(function(event){
        event.preventDefault();
        var verif = false;
        $('input').each(function(index){
            if($(this).attr("id")=="app")
            {
                verif=true
            }
            else if($(this).attr("class")=="form-control ok")
            {
                verif=true;
            }
            else
            {
                alert('veuillez remplir le formulaire');
                verif=false;
                return false;
            }
        });
    if(verif)
    {
        $('select').each(function(index){
            if($(this).attr("class")=="custom-select d-block w-100 ok")
            {
                verif=true;
            }
            else
            {
                alert('veuillez remplir le formulaire');
                verif=false;
                return false;
            }
        });
    }
    
    if(verif)
    {
        $('form').submit();
    }
        
    });

    $("#btnSend").click(function(event){
        event.preventDefault();
        var verif = false;
        $('input').each(function(index){
            if($(this).attr("id")=="vaccin")
            {
                return true;
            }
            if($(this).attr("class")=="form-control ok")
            {
                verif=true;
            }
            else
            {
                alert('veuillez remplir le formulaire');
                verif=false;
                return false;
            }
        });
    if(verif)
    {
        $('select').each(function(index){
            if($(this).attr("class")=="custom-select d-block w-100 ok")
            {
                verif=true;
            }
            else
            {
                alert('veuillez remplir le formulaire comme il faut');
                verif=false;
                return false;
            }
        });
    }
    
    if(verif)
    {
        $('form').submit();
    }
        
    });

    $("input").change(function(){
        if($(this).attr("id")=="app")
        {
            
        }
        else
        {
            if($.trim($(this).val())!="")
            {
                if($(this).attr("id")=="pwdconf")
                {
                    if($("#password").val()!=$(this).val())
                    {
                        alert("Les mots de passe doivent etre identique");
                        $(this).css('border', '3px solid red');
                        if($(this).attr("class")!="form-control ok")
                        {
                            $(this).addClass("non");
                        }
                        else
                        {
                            $(this).attr("class","form-control non");
                        }
                        
                    }
                    else
                    {
                        $(this).css('border', '3px solid green');
                        if($(this).attr("class")!="form-control non")
                        {
                            $(this).addClass("ok");
                        }
                        else
                        {
                            $(this).attr("class","form-control ok");
                        }
                       
                    }
                }
                else if($(this).attr("id")=="email")
                {
                    if($(this).val().indexOf('@')>-1 && $(this).val().indexOf('.')>-1)
                    {
                        $.post("/inscrire/email",{email:$("#email").val()},function(data){
                            if(data)
                            {
                                $("#email").css('border', '3px solid green');
                                if($("#email").attr("class")!="form-control non")
                                {
                                    $("#email").addClass("ok");
                                }
                                else
                                {
                                    $("#email").attr("class","form-control ok");
                                }
                            }
                            else
                            {
                                alert("Le email est déjà utilisé")
                                $("#email").css('border', '3px solid red');
                                if($("#email").attr("class")!="form-control ok")
                                {
                                    $("#email").addClass("non");
                                }
                                else
                                {
                                    $("#email").attr("class","form-control non");
                                }
                            }
                            
                        });
                    }
                    else
                    {
                        alert("Le format du email doit etre abc@domain.com");
                        $(this).css('border', '3px solid red');
                        if($(this).attr("class")!="form-control ok")
                        {
                            $(this).addClass("non");
                        }
                        else
                        {
                            $(this).attr("class","form-control non");
                        }
                    }
                }
                else if($(this).attr('id')=='zip')
                {
                    if(RegExp("[A-Z][0-9][A-Z] [0-9][A-Z][0-9]").test($(this).val()))
                    {
                        $(this).css('border', '3px solid green');
                        if($(this).attr("class")!="form-control non")
                        {
                            $(this).addClass("ok");
                        }
                        else
                        {
                            $(this).attr("class","form-control ok");
                        }
                    }
                    else
                    {
                        alert("Le  format du code postal doit être H0H 0H0 et les lettres doivent être en majuscule");
                        $(this).css('border', '3px solid red');
                        if($(this).attr("class")!="form-control ok")
                        {
                            $(this).addClass("non");
                        }
                        else
                        {
                            $(this).attr("class","form-control non");
                        }
                    }
                }
                else
                {
                    $(this).css('border', '3px solid green');
                    if($(this).attr("class")!="form-control non")
                    {
                        $(this).addClass("ok");
                    }
                    else
                    {
                        $(this).attr("class","form-control ok");
                    }
                }
                
            }
            else
            {
                alert("Le champ ne peut pas être vide");
                $(this).css('border', '3px solid red');
                if($(this).attr("class")!="form-control ok")
                {
                    $(this).addClass("non");
                }
                else
                {
                    $(this).attr("class","form-control non");
                }
            }
        }
    });


    
    $("select").change(function(){
        if($(this).val()!="")
        {
            $(this).css('border', '3px solid green');
            if($(this).attr("class")!="custom-select d-block w-100 non")
            {
                $(this).addClass("ok");
            }
            else
            {
                $(this).attr("class","custom-select d-block w-100 ok");
            }
        }
        else
        {
            alert("Le choix du selecteur est vide");
            $(this).css('border', '3px solid red');
            if($(this).attr("class")!="custom-select d-block w-100 ok")
            {
                $(this).addClass("non");
            }
            else
            {
                $(this).attr("class","custom-select d-block w-100 non");
            }
        }
    });
});