<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="author" content="UIdeck">
  <title>JobX - Bootstrap HTML5 Job Portal Template</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/line-icons.css">
  <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="assets/css/owl.theme.default.css">
  <link rel="stylesheet" href="assets/css/slicknav.min.css">
  <link rel="stylesheet" href="assets/css/animate.css">
  <link rel="stylesheet" href="assets/css/main.css">    
  <link rel="stylesheet" href="assets/css/responsive.css">
 <link href="./js/verif-form.js" />
</head>

<body>
  <style >
    .status-available{color:#2FC332;}
.status-not-available{color:#D60202;}
  </style>
 <!-- Header Section Start -->
  <style>
    .modal {
      display: none; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      padding-top: 100px; /* Location of the box */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
      background-color: #fefefe;
      margin: auto;
      padding: 20px;
      border: 1px solid #888;
      width: 80%;
    }

    /* The Close Button */
    .close {
      color: #aaaaaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }

    .close:hover,
    .close:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
    }
  </style>
 
 <?php
include 'connexion.php'; 

 if (isset($_POST['inscription'])){
  $nom= ($_POST["nom"]);
  $prenom= ($_POST["prenom"]);
  $username= ($_POST["username"]);
  $password= ($_POST["password"]);
  $email= ($_POST["email"]);
  $adresse= ($_POST["adresse"]);
  $pays= ($_POST["pays"]);
  $province= ($_POST["province"]);
  $ville= ($_POST["ville"]);
  $pobox= ($_POST["pobox"]);
  $qualifications= ($_POST["qualifications"]);

  $uploaddir = './uploads/';
$uploadfile = $uploaddir . basename($_FILES['file']['name']);
 $fileName = $_FILES['file']['name'];

move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
    
   


  $sql =("insert into  personne (username,password,email,pays,ville,province,adresse) values ('$username','$password','$email','$pays','$ville','$province','$adresse');insert into employe (username_employe,qualifications,cv) values('$username','$qualifications','$fileName')");
  if( mysqli_multi_query($connect,$sql)){
    ?>
    
 <div id='myModal' class='modal'>

  <!-- Modal content -->
  <div class='modal-content'>
    <span class='close'>&times;</span>
    <center>
    <h6>vous avez bien été inscrit! Vous serez redirigez vers la page d'authentification</h6>
    <div style='width:100%'>
  <input type='button' value='ok' class='btn btn-common log-btn' style='idth:20%; ' onclick="window.location.href='login.php'">
  </center>
</div>
  </div>
    <script>
    var modal = document.getElementById('myModal');
     modal.style.display = 'block';
     </script>";

<?php
  }
  else{
    echo"erreur de requete sql";
  }
         
}

?>

<header id="home" class="hero-area"> 
  <!-- Navbar Start -->
  <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
      <div class="container">
        <div class="theme-header clearfix">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="lni-menu"></span>
              <span class="lni-menu"></span>
              <span class="lni-menu"></span>
            </button>
            <!-- <a href="index.html" class="navbar-brand"><img src="assets/img/logo.png" alt=""></a>-->
            <a href="index.php"><h1 style="color: #fed136; font-family: Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';">AJob</h1></a>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item">
                
              </li>
              <a class="nav-link" href="Apropos.php">
                  A propos
                </a>

              <li class="nav-item">
                <a class="nav-link" href="contact.html">
                  Contact
                </a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="inscription.php">Inscrivez-vous</a>
              </li>
              <li class="button-group">
                <a href="LoginEmployeur.php" class="button btn btn-common">Postez un emploi</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="mobile-menu" data-logo="assets/img/logo-mobile.png"></div>
    </nav>
  <!-- Navbar End -->            
</header>

<img class="mb-4" src="mtessipr-std.png" alt="" width="250" height="250">

<section id="content" class="section-padding">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-5 col-md-6 col-xs-12" style="    flex: 0 0 90.666667%;
      max-width: 90.666667%;">
      <div class="page-login-form box">
        <h4 class="mb-3">Formulaire d'inscription</h4>
        <form class="needs-validation" novalidate="" method="post" enctype="multipart/form-data">
          <div class="row">

            <div class="col-md-6 mb-3">
              <label for="firstName">Prénom</label>
              <input type="text" class="form-control" id="firstName" placeholder="Prénom" value="" required="required" name="prenom">
              <div class="invalid-feedback">
                Veuillez entrez un prénom
              </div>
            </div>
            <div class="col-md-6 mb-3">
              <label for="lastName">Nom</label>
              <input type="text" class="form-control" id="lastName" placeholder="Nom" value="" required="Required" name="nom">
              <div class="invalid-feedback">
                Veuillez entrez un nom
              </div>
            </div>
          </div>

          <div class="mb-3">

            <label for="username">Utilisateur</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="lni-user"></i></span>
              </div>
              <input type="text" class="form-control" id="username" placeholder="Utilisateur" required="" onBlur="checkAvailability()" name="username"><span id="user-availability-status"></span>
              <div class="invalid-feedback" style="width: 100%;">
                Veuillez entrez un nom d'utilisateur valide
              </div>
            </div>
          </div>
          
          <div class="mb-3">
            <label for="username">Mot de passe</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="lni-lock"></i></span>
              </div>
              <input type="password" class="form-control" id="password" placeholder="Mot de passe" required="required" name="password">
              <div class="invalid-feedback" style="width: 100%;">
                Veuillez entrez un mot de passe d'au moins 8 caractères dont au moins une lettre et un chiffre
              </div>
            </div>
          </div>

          <div class="mb-3">
            <label for="username">Confirmation Mot de passe</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="lni-lock"></i></span>
              </div>
              <input type="password" class="form-control" id="pwdconf" placeholder="Confirmation Mot de passe" required="">
              <div class="invalid-feedback" style="width: 100%;">
                Le mot de passe doit être identique
              </div>
            </div>
          </div>
          <div class="mb-3">
            <label for="email">Courriel <span class="text-muted"></span></label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">@</span>
              </div>
              
              <input type="email" class="form-control" id="email" placeholder="you@example.com" required="" name="email">
              <div class="invalid-feedback">
                Veuillez entrez une adresse courriel valide
              </div>
            </div>
          </div>

          <div class="mb-3">
            <label for="address">Addresse</label>
            <input type="text" class="form-control" id="address" placeholder="1234 Main St" required="" name="adresse">
            <div class="invalid-feedback">
              Veuillez entrez une adresse
            </div>
          </div>

          <div class="mb-3">
            <label for="address2">Appartement <span class="text-muted">(Optional)</span></label>
            <input type="text" class="form-control" id="address2" placeholder="Appartement ou suite">
          </div>

          <div class="row">
            <div class="col-md-5 mb-3">
              <label for="country">Pays</label>
              <select class="custom-select d-block w-100" id="country" required="" name="pays">
                <option value="">Choix...</option>
                <option>Canada</option>
              </select>
              <div class="invalid-feedback">
                Veuillez entrez un pays
              </div>
            </div>
            <div class="col-md-4 mb-3">
              <label for="state">Province</label>
              <select class="custom-select d-block w-100" id="state" required="" name="province">
                <option value="">Choix...</option>
                <option>Québec</option>
              </select>
              <div class="invalid-feedback">
                Veuillez entrez une province
              </div>
            </div>
            <div class="col-md-3 mb-3">
              <label for="zip">Code Postal</label>
              <input type="text" class="form-control" id="zip" placeholder="H0H 0H0" required="" name="pobox">
              <div class="invalid-feedback">
                Veuillez entrez un code postal valide (H0H 0H0)
              </div>
            </div>
            
            <div class="col-md-3 mb-3">
              <label for="zip">Ville</label>
              <input type="text" class="form-control" id="zip" placeholder="Yamaska" required="" name="ville">
              <div class="invalid-feedback">
                Veuillez entrez une ville valide
              </div>
            </div>
            <br>
            
          </div>

          
          <div class="mb-3">
              <label for="zip">Qualifications</label>
              
              <textarea class="form-control"  required="" name="qualifications"></textarea>
           
          </div>

          <div class="input-group">
            <div class="col-md-3 mb-3">
              <label for="zip">CV</label>
              <input type="file" class="form-control"  required="" name="file" >
              
            </div>
          </div>

          <hr class="mb-4">
          <input type="submit" name="inscription" class="btn btn-common log-btn" value="Inscription">
        </form>
      </div>

    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
<script>
function checkAvailability() {
  jQuery.ajax({
  url: "verif.php",
  data:'username='+$("#username").val(),
  type: "POST",
  success:function(data){
    $("#user-availability-status").html(data);
  },
  error:function (){}
  }); 
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>




<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="form-avalidation.js"></script>

<script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../js/main.js"></script>
   <script src="./js/verif-form.js"></script>

</body>