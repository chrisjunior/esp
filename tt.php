<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="author" content="UIdeck">
  <title>JobX - Bootstrap HTML5 Job Portal Template</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/line-icons.css">
  <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="assets/css/owl.theme.default.css">
  <link rel="stylesheet" href="assets/css/slicknav.min.css">
  <link rel="stylesheet" href="assets/css/animate.css">
  <link rel="stylesheet" href="assets/css/main.css">    
  <link rel="stylesheet" href="assets/css/responsive.css">

</head>

<body>
  <style >
    .status-available{color:#2FC332;}
.status-not-available{color:#D60202;}
  </style>
 <!-- Header Section Start -->
  <style>
    .modal {
      display: none; /* Hidden by default */
      position: fixed; /* Stay in place */
      z-index: 1; /* Sit on top */
      padding-top: 100px; /* Location of the box */
      left: 0;
      top: 0;
      width: 100%; /* Full width */
      height: 100%; /* Full height */
      overflow: auto; /* Enable scroll if needed */
      background-color: rgb(0,0,0); /* Fallback color */
      background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
      background-color: #fefefe;
      margin: auto;
      padding: 20px;
      border: 1px solid #888;
      width: 80%;
    }

    /* The Close Button */
    .close {
      color: #aaaaaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }

    .close:hover,
    .close:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
    }
  </style>
 
 <?php
include 'connexion.php'; 

 if (isset($_POST['inscription'])){
  $nom= ($_POST["nom"]);
 
  $email= ($_POST["email"]);
  $adresse= ($_POST["adresse"]);
  
  $cat= ($_POST["cat"]);
  $ville= ($_POST["ville"]);
 
  $tel= ($_POST["tel"]);

  $uploaddir = './uploads/';
$uploadfile = $uploaddir . basename($_FILES['file']['name']);
 $filename = $_FILES['file']['name'];

move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
    
   

     $sql =("update fournisseur set adresse='$adresse', ville='$ville', tel='$tel', courriel='$email', logo='$filename', cat='$cat' where nom='$nom'");
 /* if( mysqli_multi_query($connect,$sql)){*/
       if( mysqli_num_rows($sql)>0){
    
    ?>
    
  <div id='myModal' class='modal'>

  <!-- Modal content -->
  <div class='modal-content'>
    <span class='close'>&times;</span>
    <center>
    <h6>vous avez bien effectué votre mise à jour! Vous serez redirigez vers la page d'authentification</h6>
    <div style='width:100%'>
  <input type='button' value='ok' class='btn btn-common log-btn' style='idth:20%; ' onclick="window.location.href='login.php'">
  </center>
</div>
  </div>
    <script>
    var modal = document.getElementById('myModal');
     modal.style.display = 'block';
     </script>";
    

<?php
  }
  else{
    echo"alert('erreur de requete sql')";
  }
         
}

?>

<header id="home" class="hero-area"> 
  <!-- Navbar Start -->
  <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
    <div class="container">
      <div class="theme-header clearfix">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            <span class="lni-menu"></span>
            <span class="lni-menu"></span>
            <span class="lni-menu"></span>
          </button>
          <!-- <a href="index.html" class="navbar-brand"><img src="assets/img/logo.png" alt=""></a>-->
          <h1 style="color: #fed136; font-family: Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';">Catalog</h1>
        </div>
        <div class="collapse navbar-collapse" id="main-navbar">
          <ul class="navbar-nav mr-auto w-100 justify-content-end">
            <li class="nav-item">
              <a class="nav-link" href="index.html">
                Acceuil
              </a>
            </li>
           
           
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Categories
              </a>
              <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="browse-jobs.html">browse-categories</a></li>
                <li><a class="dropdown-item" href="browse-categories.html">Browse Categories</a></li>
                <li><a class="dropdown-item" href="add-resume.html">Add Resume</a></li>
                <li><a class="dropdown-item" href="manage-resumes.html">Manage Resumes</a></li>
                <li><a class="dropdown-item" href="job-alerts.html">Job Alerts</a></li>
              </ul>
            </li>
            
            
            <li class="nav-item">
              <a class="nav-link" href="contact.html">
                Contact
              </a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="login.html">Inscrivez-vous</a>
            </li>
            <li class="button-group">
              <a href="post-job.html" class="button btn btn-common">Postez un emploi</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="mobile-menu" data-logo="assets/img/logo-mobile.png"></div>
  </nav>
  <!-- Navbar End -->            
</header>

<img class="mb-4" src="mtessipr-std.png" alt="" width="250" height="250">

<section id="content" class="section-padding">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-5 col-md-6 col-xs-12" style="    flex: 0 0 90.666667%;
      max-width: 90.666667%;">
      <div class="page-login-form box">
        <h4 class="mb-3">Formulaire de modification</h4>
        <form class="needs-validation" novalidate="" method="post" enctype="multipart/form-data">
          
           
            <div class="mb-3">
              <label for="lastName">Nom</label>
             
                <select name="nom">  
   <?php	
	$requete=mysqli_query($connect,"select * from fournisseur;")or die("Erreur de requete SQL"); 
	 while($resultat=mysqli_fetch_row($requete))
                {
					$code = $resultat[1];
					$choix=$_POST['nom'];
					 
                    if($choix==$code)
					{
						$selected="selected";
					}
                    else
                    {
						$selected="";
                    }            
                  echo "<option  value='$code' $selected >$code</option>";
                } 
	?>
   </select>
            </div>
          

          
         

          <div class="mb-3">
            <label for="email">Courriel <span class="text-muted"></span></label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">@</span>
              </div>
              
              <input type="email" class="form-control" id="email" placeholder="you@example.com" required="" name="email">
              <div class="invalid-feedback">
                Veuillez entrez une adresse courriel valide
              </div>
            </div>
          </div>

          <div class="mb-3">
            <label for="address">Addresse</label>
            <input type="text" class="form-control" id="address" placeholder="1234 Main St" required="" name="adresse">
            <div class="invalid-feedback">
              Veuillez entrez une adresse
            </div>
          </div>

           <div class="mb-3">
            <label for="address">Ville</label>
            <input type="text" class="form-control" id="zip" placeholder="Yamaska" required="" name="ville">
            <div class="invalid-feedback">
              Veuillez entrez une ville
            </div>
          </div>
            
             <div class="mb-3">
            <label for="address">telephone</label>
            <input type="text" class="form-control" id="tele" placeholder="111-111-1111" required="" name="tel">
            <div class="invalid-feedback">
              Veuillez entrez un numero de telephone
            </div>
          </div>

            
             
               <div class="mb-3">
            <label for="address">Categorie</label>
            <input type="text" class="form-control" id="cate" placeholder="planche" required="" name="cat">
            <div class="invalid-feedback">
              Veuillez entrez une categorie
            </div>
          </div>


           <div class="input-group">
            <div class="col-md-3 mb-3">
              <label for="zip">logo</label>
              <input type="file" class="form-control"  required="" name="file" >
              
            </div>
          </div>


         

        
          </div>

          <hr class="mb-4">
          <input type="submit" name="inscription" class="btn btn-common log-btn" value="Modifier">
        </form>
      

    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
<script>
function checkAvailability() {
  jQuery.ajax({
  url: "verif.php",
  data:'username='+$("#username").val(),
  type: "POST",
  success:function(data){
    $("#user-availability-status").html(data);
  },
  error:function (){}
  }); 
}
</script>
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

<script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="form-avalidation.js"></script>

</body>