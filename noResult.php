<?php 
session_start()
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="keywords" content="Bootstrap, Landing page, Template, Registration, Landing">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="author" content="UIdeck">
  <title>JobX - Bootstrap HTML5 Job Portal Template</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/css/line-icons.css">
  <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="assets/css/owl.theme.default.css">
  <link rel="stylesheet" href="assets/css/slicknav.min.css">
  <link rel="stylesheet" href="assets/css/animate.css">
  <link rel="stylesheet" href="assets/css/main.css">    
  <link rel="stylesheet" href="assets/css/responsive.css">

</head>

<body>

  <!-- Header Section Start -->
  <header id="home" class="hero-area"> 
    <!-- Navbar Start -->
        <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
      <div class="container">
        <div class="theme-header clearfix">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="lni-menu"></span>
              <span class="lni-menu"></span>
              <span class="lni-menu"></span>
            </button>
            <!-- <a href="index.html" class="navbar-brand"><img src="assets/img/logo.png" alt=""></a>-->
            <a href="acceuil.php"><h1 style="color: #fed136; font-family: Montserrat,-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';">Catalog</h1></a>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav mr-auto w-100 justify-content-end">
              <li class="nav-item">
                
              </li>
              <a class="nav-link" href="Apropos.php">
                  A propos
                </a>

              <li class="nav-item">
                <a class="nav-link" href="contact.php">
                  Contact
                </a>
              </li>
        
            <li class="button-group">
              <a href="compteEmploye.php" class="button btn btn-common"><?php echo $_SESSION["username"]?></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="mobile-menu" data-logo="assets/img/logo-mobile.png"></div>
  </nav>
    <!-- Navbar End -->            
  </header>
  <!-- Header Section End -->

  <!-- Page Header Start -->
  <div class="page-header">
    <div class="container">
      <div class="row">         
        <div class="col-lg-12">
          <div class="inner-header">
            <h3>Recherchez un fournisseur</h3>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Page Header End --> 

  <section id="latest-jobs" class="section bg-gray">
    <div class="container">
      <div class="section-header">  
        <h2 class="section-title">Aucun résultats trouvés!! Veuillez réessayer</h2>
        <a href="acceuil.php">Cliquez ici pour recommencer</a>
        
      </section>
  <!-- Latest Section End -->

  
  <!-- Footer Section Start -->
  <footer>
     <div id="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="site-info text-center">
              <p>Designed and Developed by <a href="https://uideck.com" rel="nofollow">Christian Junior Djomga</a></p>
            </div>     
          </div>
        </div>
      </div>
         </div>
      </footer>
  <!-- Footer Section End -->  

  <!-- Go To Top Link -->
  <a href="#" class="back-to-top">
    <i class="lni-arrow-up"></i>
  </a> 

  <!-- Preloader -->
  <div id="preloader">
    <div class="loader" id="loader-1"></div>
  </div>
  <!-- End Preloader -->

  <!-- jQuery first, then Tether, then Bootstrap JS. -->
  <script src="assets/js/jquery-min.js"></script>
  <script src="assets/js/popper.min.js"></script>

  <script src="assets/js/owl.carousel.min.js"></script>     
  <script src="assets/js/jquery.slicknav.js"></script>     
  <script src="assets/js/jquery.counterup.min.js"></script>      
  <script src="assets/js/waypoints.min.js"></script>     
  <script src="assets/js/form-validator.min.js"></script>
  <script src="assets/js/contact-form-script.js"></script>   
  <script src="assets/js/main.js"></script>
  
</body>
    </html>